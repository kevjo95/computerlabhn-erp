// ==============================[DEPENDENCIAS]==============================
var express = require("express");
var morgan = require('morgan');
var bodyParser = require('body-parser');
var session = require('express-session');
var rutas = require('./rutas');

// ==============================[INSTANCIA SERVIDOR]==============================
var app = express();

// ==============================[MIDDLEWARES]==============================
app.use(morgan('dev'));
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
// parse application/json
app.use(bodyParser.json());
//sessiones
app.use(session({secret:"ASDFSDF$%%aasdera", resave: true, saveUninitialized:true}));

// ==============================[ARCHIVOS ESTATICOS]==============================
app.use(express.static("app"));
// ================================[RUTA PADRE]====================================
app.use("/",rutas);
// =================================[PUERTO]=======================================
app.set('port', 3030);
app.listen(app.get('port'), ()=>{
    console.log(`Server on port ${app.get('port')}`);
});

// ==============================[RUTAS GESTIONADAS]==============================
require('./server/routes/holaMundo.route')(app);
require('./server/routes/session.route')(app);
require('./server/routes/tiposArticulos.route')(app);
require('./server/routes/estados.route')(app);
require('./server/routes/inventario.route')(app);
require('./server/routes/compras.route')(app);
require('./server/routes/envios.route')(app);
require('./server/routes/articulos.route')(app);