-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 19-12-2018 a las 06:17:31
-- Versión del servidor: 5.7.21
-- Versión de PHP: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_computerlabhn_erp`
--

DELIMITER $$
--
-- Procedimientos
--
DROP PROCEDURE IF EXISTS `sp_insertar_articulo_inv`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insertar_articulo_inv` (`p_id_tipo` INT, `p_id_estado` INT, `p_id_estado_favorito` INT, `p_articulo` VARCHAR(200), `p_descripcion` VARCHAR(200), `p_cantidad` INT, `p_precio_compra` DECIMAL(10,2), `p_precio_venta` DECIMAL(10,2), `p_comentarios` VARCHAR(200), `p_enlace` VARCHAR(4000), `p_id_usuario_crea` INT)  BEGIN
	INSERT INTO `db_computerlabhn_erp`.`tbl_inventario_art`
		(
			`id_tipo`,
			`id_estado`,
			`id_estado_favorito`,
			`articulo`,
			`descripcion`,
			`cantidad`,
			`precio_compra`,
			`precio_venta`,
			`comentarios`,
			`enlace`,
			`fecha_crea`,
			`id_usuario_crea`
		)
	VALUES
		(
			p_id_tipo,
			p_id_estado,
			p_id_estado_favorito,
			p_articulo,
			p_descripcion,
			p_cantidad,
			p_precio_compra,
			p_precio_venta,
			p_comentarios,
			p_enlace,
			now(),
			p_id_usuario_crea
		);
END$$

--
-- Funciones
--
DROP FUNCTION IF EXISTS `fx_validar_login`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `fx_validar_login` (`p_usuario` VARCHAR(10), `p_contrasenia` VARCHAR(10)) RETURNS TINYINT(1) BEGIN
	DECLARE v_acceso BOOLEAN default FALSE;
    
	SET v_acceso = (SELECT CASE WHEN sha1(p_contrasenia) = contrasenia THEN TRUE ELSE FALSE END FROM tbl_usuarios WHERE nombre_usuario = p_usuario); 
     
RETURN v_acceso;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_articulos_online`
--

DROP TABLE IF EXISTS `tbl_articulos_online`;
CREATE TABLE IF NOT EXISTS `tbl_articulos_online` (
  `id_articulo` int(11) NOT NULL AUTO_INCREMENT,
  `articulo` varchar(200) NOT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `enlace` varchar(1000) DEFAULT NULL,
  `id_estado` int(11) NOT NULL,
  PRIMARY KEY (`id_articulo`),
  KEY `fk_tbl_articulos_online_tbl_estado_actividad1_idx` (`id_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_art_x_compra`
--

DROP TABLE IF EXISTS `tbl_art_x_compra`;
CREATE TABLE IF NOT EXISTS `tbl_art_x_compra` (
  `id_compra` int(11) NOT NULL,
  `id_art_online` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio_unidad` decimal(10,2) DEFAULT NULL,
  `impuesto` decimal(10,2) DEFAULT NULL,
  `gastos_envio` decimal(10,2) DEFAULT NULL,
  `sub_total` decimal(10,2) DEFAULT NULL,
  `comision` decimal(10,2) DEFAULT NULL,
  `total` decimal(10,2) DEFAULT NULL,
  `comentario` varchar(1000) DEFAULT NULL,
  KEY `fk_tbl_art_x_compra_tbl_articulos_online1_idx` (`id_art_online`),
  KEY `fk_tbl_art_x_compra_tbl_compras1_idx` (`id_compra`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_art_x_cotizacion`
--

DROP TABLE IF EXISTS `tbl_art_x_cotizacion`;
CREATE TABLE IF NOT EXISTS `tbl_art_x_cotizacion` (
  `id_cotizacion` int(11) NOT NULL,
  `id_art_inv` int(11) NOT NULL,
  `id_art_online` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio_unidad` decimal(10,2) DEFAULT NULL,
  `impuesto` decimal(10,2) DEFAULT NULL,
  `gastos_envio` decimal(10,2) DEFAULT NULL,
  `sub_total` decimal(10,2) DEFAULT NULL,
  `comision` decimal(10,2) DEFAULT NULL,
  `total` decimal(10,2) DEFAULT NULL,
  `comentario` varchar(1000) DEFAULT NULL,
  KEY `fk_tbl_art_x_cotizacion_tbl_inventario_art1_idx` (`id_art_inv`),
  KEY `fk_tbl_art_x_cotizacion_tbl_cotizaciones1_idx` (`id_cotizacion`),
  KEY `fk_tbl_art_x_cotizacion_tbl_articulos_online1_idx` (`id_art_online`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_art_x_pedido`
--

DROP TABLE IF EXISTS `tbl_art_x_pedido`;
CREATE TABLE IF NOT EXISTS `tbl_art_x_pedido` (
  `id_pedido` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_articulo_online` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio_unidad` decimal(10,2) DEFAULT NULL,
  `impuesto` decimal(10,2) DEFAULT NULL,
  `gastos_envio` decimal(10,2) DEFAULT NULL,
  `sub_total` decimal(10,2) DEFAULT NULL,
  `comision` decimal(10,2) DEFAULT NULL,
  `total` decimal(10,2) DEFAULT NULL,
  `comentario` varchar(1000) DEFAULT NULL,
  KEY `fk_tbl_art_x_pedido_tbl_articulos_online1_idx` (`id_articulo_online`),
  KEY `fk_tbl_art_x_pedido_tbl_pedidos1_idx` (`id_pedido`),
  KEY `fk_tbl_art_x_pedido_tbl_clientes1_idx` (`id_cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_clientes`
--

DROP TABLE IF EXISTS `tbl_clientes`;
CREATE TABLE IF NOT EXISTS `tbl_clientes` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(45) NOT NULL,
  `apellidos` varchar(45) NOT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `facebook_enlace` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_compras`
--

DROP TABLE IF EXISTS `tbl_compras`;
CREATE TABLE IF NOT EXISTS `tbl_compras` (
  `id_compra` int(11) NOT NULL AUTO_INCREMENT,
  `id_envio` int(11) NOT NULL,
  `codigo` varchar(45) DEFAULT NULL,
  `fecha_crea` datetime DEFAULT NULL,
  `usuario_crea` int(11) DEFAULT NULL,
  `fecha_modifica` datetime DEFAULT NULL,
  `usuario_modifica` int(11) DEFAULT NULL,
  `id_estado_aprovacion` int(11) NOT NULL,
  PRIMARY KEY (`id_compra`),
  KEY `fk_tbl_compras_tbl_envios1_idx` (`id_envio`),
  KEY `fk_tbl_compras_tbl_estado_aprovacion1_idx` (`id_estado_aprovacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_cotizaciones`
--

DROP TABLE IF EXISTS `tbl_cotizaciones`;
CREATE TABLE IF NOT EXISTS `tbl_cotizaciones` (
  `id_cotizacion` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) NOT NULL,
  `codigo` varchar(45) DEFAULT NULL,
  `presupuesto` decimal(10,2) DEFAULT NULL,
  `fecha_crea` datetime DEFAULT NULL,
  `id_usuario_crea` int(11) DEFAULT NULL,
  `fecha_modifica` datetime DEFAULT NULL,
  `id_usuario_modifica` int(11) DEFAULT NULL,
  `id_estado_aprovacion` int(11) NOT NULL,
  PRIMARY KEY (`id_cotizacion`),
  KEY `fk_tbl_cotizaciones_tbl_clientes1_idx` (`id_cliente`),
  KEY `fk_tbl_cotizaciones_tbl_estado_aprovacion1_idx` (`id_estado_aprovacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_empleados`
--

DROP TABLE IF EXISTS `tbl_empleados`;
CREATE TABLE IF NOT EXISTS `tbl_empleados` (
  `id_empleado` int(11) NOT NULL AUTO_INCREMENT,
  `num_identidad` varchar(45) DEFAULT NULL,
  `nombres` varchar(45) NOT NULL,
  `apellidos` varchar(45) NOT NULL,
  `fecha_cumpleanios` datetime DEFAULT NULL,
  `correo` varchar(45) NOT NULL,
  `fecha_creado` datetime DEFAULT NULL,
  `fecha_modificado` datetime DEFAULT NULL,
  `id_estado` int(11) NOT NULL,
  PRIMARY KEY (`id_empleado`),
  KEY `fk_tbl_empleados_tbl_estados1_idx` (`id_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_envios`
--

DROP TABLE IF EXISTS `tbl_envios`;
CREATE TABLE IF NOT EXISTS `tbl_envios` (
  `id_envio` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(45) NOT NULL,
  `fecha_miami` datetime DEFAULT NULL,
  `fecha_honduras` datetime DEFAULT NULL,
  `gasto_envio` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id_envio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_estado`
--

DROP TABLE IF EXISTS `tbl_estado`;
CREATE TABLE IF NOT EXISTS `tbl_estado` (
  `id_estado` int(11) NOT NULL AUTO_INCREMENT,
  `estado` varchar(45) NOT NULL,
  PRIMARY KEY (`id_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_estado`
--

INSERT INTO `tbl_estado` (`id_estado`, `estado`) VALUES
(1, 'ACTIVO'),
(2, 'INACTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_estado_aprobacion`
--

DROP TABLE IF EXISTS `tbl_estado_aprobacion`;
CREATE TABLE IF NOT EXISTS `tbl_estado_aprobacion` (
  `id_estado` int(11) NOT NULL AUTO_INCREMENT,
  `estado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_estado_aprobacion`
--

INSERT INTO `tbl_estado_aprobacion` (`id_estado`, `estado`) VALUES
(1, 'APROBADO'),
(2, 'PENDIENTE'),
(3, 'RECHAZADO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_historial_accesos`
--

DROP TABLE IF EXISTS `tbl_historial_accesos`;
CREATE TABLE IF NOT EXISTS `tbl_historial_accesos` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `fecha_ingreso` datetime NOT NULL,
  PRIMARY KEY (`id_log`),
  KEY `fk_tbl_historial_accesos_tbl_usuarios1_idx` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_historial_accesos`
--

INSERT INTO `tbl_historial_accesos` (`id_log`, `id_usuario`, `fecha_ingreso`) VALUES
(2, 3, '2018-12-18 17:34:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_inventario_art`
--

DROP TABLE IF EXISTS `tbl_inventario_art`;
CREATE TABLE IF NOT EXISTS `tbl_inventario_art` (
  `id_articulo` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipo` int(11) NOT NULL,
  `id_estado` int(11) NOT NULL,
  `id_estado_favorito` int(11) NOT NULL,
  `articulo` varchar(200) NOT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_compra` decimal(10,8) NOT NULL,
  `precio_venta` decimal(10,8) DEFAULT NULL,
  `comentarios` varchar(200) DEFAULT NULL,
  `enlace` varchar(4000) DEFAULT NULL,
  `fecha_crea` datetime NOT NULL,
  `id_usuario_crea` int(11) NOT NULL,
  `fecha_modifica` datetime DEFAULT NULL,
  `id_usuario_modifica` int(11) NOT NULL,
  PRIMARY KEY (`id_articulo`),
  KEY `fk_tbl_inventario_tbl_tipo_art1_idx` (`id_tipo`),
  KEY `fk_tbl_inventario_tbl_estados1_idx` (`id_estado`),
  KEY `fk_tbl_inventario_tbl_usuarios1_idx` (`id_usuario_crea`),
  KEY `fk_tbl_inventario_tbl_usuarios2_idx` (`id_usuario_modifica`),
  KEY `fk_tbl_inventario_art_tbl_estado1_idx` (`id_estado_favorito`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_parametros_conf`
--

DROP TABLE IF EXISTS `tbl_parametros_conf`;
CREATE TABLE IF NOT EXISTS `tbl_parametros_conf` (
  `id_parametro` int(11) NOT NULL AUTO_INCREMENT,
  `parametro` varchar(45) DEFAULT NULL,
  `descripcion` varchar(1000) DEFAULT NULL,
  `valor_num` decimal(10,2) DEFAULT NULL,
  `valor_txt` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_parametro`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_pedidos`
--

DROP TABLE IF EXISTS `tbl_pedidos`;
CREATE TABLE IF NOT EXISTS `tbl_pedidos` (
  `id_pedido` int(11) NOT NULL AUTO_INCREMENT,
  `id_envio` int(11) NOT NULL,
  `codigo` varchar(45) DEFAULT NULL,
  `fecha_crea` datetime DEFAULT NULL,
  `usuario_crea` int(11) DEFAULT NULL,
  `fecha_modifica` datetime DEFAULT NULL,
  `usuario_modifica` int(11) DEFAULT NULL,
  `id_estado_aprovacion` int(11) NOT NULL,
  PRIMARY KEY (`id_pedido`),
  KEY `fk_tbl_pedidos_tbl_envios1_idx` (`id_envio`),
  KEY `fk_tbl_pedidos_tbl_estado_aprovacion1_idx` (`id_estado_aprovacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_tipo_art`
--

DROP TABLE IF EXISTS `tbl_tipo_art`;
CREATE TABLE IF NOT EXISTS `tbl_tipo_art` (
  `id_tipo` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(45) NOT NULL,
  PRIMARY KEY (`id_tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_tipo_art`
--

INSERT INTO `tbl_tipo_art` (`id_tipo`, `tipo`) VALUES
(1, 'PROCESADOR'),
(2, 'TARJETA GRAFICA'),
(3, 'TARJETA MADRE'),
(4, 'MEMORIA'),
(5, 'ALMACENAMIENTO'),
(6, 'ENFRIAMIENTO'),
(7, 'GABINETE'),
(8, 'VENTILADORES'),
(9, 'PERIFERICOS'),
(10, 'OTROS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_usuarios`
--

DROP TABLE IF EXISTS `tbl_usuarios`;
CREATE TABLE IF NOT EXISTS `tbl_usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_usuario` varchar(45) DEFAULT NULL,
  `contrasenia` varchar(45) NOT NULL,
  `id_estado` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `fk_tbl_usuarios_tbl_estados_idx` (`id_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_usuarios`
--

INSERT INTO `tbl_usuarios` (`id_usuario`, `nombre_usuario`, `contrasenia`, `id_estado`) VALUES
(1, 'JOCHOA', 'bcdcb29ed2aab16d48c11485264df665e906bdd9', 1),
(2, 'DCARCAMO', 'bcdcb29ed2aab16d48c11485264df665e906bdd9', 1),
(3, 'KOLIVA', 'bcdcb29ed2aab16d48c11485264df665e906bdd9', 1);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tbl_articulos_online`
--
ALTER TABLE `tbl_articulos_online`
  ADD CONSTRAINT `fk_tbl_articulos_online_tbl_estado_actividad1` FOREIGN KEY (`id_estado`) REFERENCES `tbl_estado` (`id_estado`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tbl_art_x_compra`
--
ALTER TABLE `tbl_art_x_compra`
  ADD CONSTRAINT `fk_tbl_art_x_compra_tbl_articulos_online1` FOREIGN KEY (`id_art_online`) REFERENCES `tbl_articulos_online` (`id_articulo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_art_x_compra_tbl_compras1` FOREIGN KEY (`id_compra`) REFERENCES `tbl_compras` (`id_compra`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tbl_art_x_cotizacion`
--
ALTER TABLE `tbl_art_x_cotizacion`
  ADD CONSTRAINT `fk_tbl_art_x_cotizacion_tbl_articulos_online1` FOREIGN KEY (`id_art_online`) REFERENCES `tbl_articulos_online` (`id_articulo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_art_x_cotizacion_tbl_cotizaciones1` FOREIGN KEY (`id_cotizacion`) REFERENCES `tbl_cotizaciones` (`id_cotizacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_art_x_cotizacion_tbl_inventario_art1` FOREIGN KEY (`id_art_inv`) REFERENCES `tbl_inventario_art` (`id_articulo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tbl_art_x_pedido`
--
ALTER TABLE `tbl_art_x_pedido`
  ADD CONSTRAINT `fk_tbl_art_x_pedido_tbl_articulos_online1` FOREIGN KEY (`id_articulo_online`) REFERENCES `tbl_articulos_online` (`id_articulo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_art_x_pedido_tbl_clientes1` FOREIGN KEY (`id_cliente`) REFERENCES `tbl_clientes` (`id_cliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_art_x_pedido_tbl_pedidos1` FOREIGN KEY (`id_pedido`) REFERENCES `tbl_pedidos` (`id_pedido`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tbl_compras`
--
ALTER TABLE `tbl_compras`
  ADD CONSTRAINT `fk_tbl_compras_tbl_envios1` FOREIGN KEY (`id_envio`) REFERENCES `tbl_envios` (`id_envio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_compras_tbl_estado_aprovacion1` FOREIGN KEY (`id_estado_aprovacion`) REFERENCES `tbl_estado_aprobacion` (`id_estado`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tbl_cotizaciones`
--
ALTER TABLE `tbl_cotizaciones`
  ADD CONSTRAINT `fk_tbl_cotizaciones_tbl_clientes1` FOREIGN KEY (`id_cliente`) REFERENCES `tbl_clientes` (`id_cliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_cotizaciones_tbl_estado_aprovacion1` FOREIGN KEY (`id_estado_aprovacion`) REFERENCES `tbl_estado_aprobacion` (`id_estado`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tbl_empleados`
--
ALTER TABLE `tbl_empleados`
  ADD CONSTRAINT `fk_tbl_empleados_tbl_estados1` FOREIGN KEY (`id_estado`) REFERENCES `tbl_estado` (`id_estado`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tbl_historial_accesos`
--
ALTER TABLE `tbl_historial_accesos`
  ADD CONSTRAINT `fk_tbl_historial_accesos_tbl_usuarios1` FOREIGN KEY (`id_usuario`) REFERENCES `tbl_usuarios` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tbl_inventario_art`
--
ALTER TABLE `tbl_inventario_art`
  ADD CONSTRAINT `fk_tbl_inventario_art_tbl_estado1` FOREIGN KEY (`id_estado_favorito`) REFERENCES `tbl_estado` (`id_estado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_inventario_tbl_estados1` FOREIGN KEY (`id_estado`) REFERENCES `tbl_estado` (`id_estado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_inventario_tbl_tipo_art1` FOREIGN KEY (`id_tipo`) REFERENCES `tbl_tipo_art` (`id_tipo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_inventario_tbl_usuarios1` FOREIGN KEY (`id_usuario_crea`) REFERENCES `tbl_usuarios` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_inventario_tbl_usuarios2` FOREIGN KEY (`id_usuario_modifica`) REFERENCES `tbl_usuarios` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tbl_pedidos`
--
ALTER TABLE `tbl_pedidos`
  ADD CONSTRAINT `fk_tbl_pedidos_tbl_envios1` FOREIGN KEY (`id_envio`) REFERENCES `tbl_envios` (`id_envio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_pedidos_tbl_estado_aprovacion1` FOREIGN KEY (`id_estado_aprovacion`) REFERENCES `tbl_estado_aprobacion` (`id_estado`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tbl_usuarios`
--
ALTER TABLE `tbl_usuarios`
  ADD CONSTRAINT `fk_tbl_usuarios_tbl_estados` FOREIGN KEY (`id_estado`) REFERENCES `tbl_estado` (`id_estado`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
