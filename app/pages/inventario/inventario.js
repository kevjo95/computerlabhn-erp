function solicitudInventario(id){
    console.log(id);
    $.ajax({
        url: "/api/inventario",
        method: "GET",
        dataType: "json",
        success:function(res){
            console.log(res);
            generarTablaInventario(res);         
        },
        error(err){
            console.log(err);
        }
    })
}

function generarTablaInventario(consulta) {
    var tbody = '';
    consulta.forEach(fila => {
        tbody += `<tr class="d-flex altura">
            <td class="col-1">#${fila.id_articulo}</td>
            <td class="col-3">${fila.articulo}</td>
            <td class="col-2">${fila.tipo_articulo}</td>
            <td class="col-1 td-number">${fila.cantidad}</td>
            <td class="col-1 td-number">${fila.precio_compra}</td>
            <td class="col-1 td-number">${fila.precio_sugerido}</td>
            <td class="col-1 td-number">${fila.precio_sugerido - fila.precio_compra}</td>
            <td class="col-1 td-number">
                <select name="" id="">
                    <option value="1">15%</option>
                    <option value="2">20%</option>
                    <option value="3">25%</option>
                </select>
            </td>
            <td class="col-1 td-number">${fila.precio_sugerido}</td>
            <!--td class="col-1" style="text-align:center;">
                <a onclick="alert('borra')" class="mr-1"><i class="fas fa-trash-alt"></i></a>
                <a onclick="alert('modifica')"><i class="fas fa-pencil-alt"></i></a>
                <a onclick="alert('favorito')"><i class="fas fa-heart"></i></a>
            </td--> 
        </tr>`;    
    });
    
    $('#tablaInventario').append(tbody);    
}