function openNav() {
    document.getElementById("mySidebar").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}

function closeNav() {
    document.getElementById("mySidebar").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
}

function router(id){
    if(id=="id1"){
        id="Home";
        ruta="dashboard/dashboard.html"  
    }if(id=="id2"){
        id="Ventas";
        ruta="ventas/ventas.html"  
    }if(id=="id3"){
        id="Inventario";
        ruta="inventario/inventario.html"  
    }if(id=="id4"){
        id="Pedidos";
        ruta="pedidos/pedidos.html"  
    }if(id=="id5"){
        id="Historial";
        ruta="historial/historial.html"  
    }if(id=="id6"){
        id="Favoritos";
        ruta="favoritos/favoritos.html"  
    }
    Deseleccionar();
    document.getElementById("opcion"+id).className="opcionSelect";
    pintarModulo(ruta,id);
};  

function pintarModulo(ruta,id){
    $.ajax({
        url:"../"+ruta,
        dataType:"text",
        success:function(data){
            document.getElementById("contenidoHome").innerHTML= data;
        }
    });

    if(id=="Home"){
        solicitudHome(id);
    }if(id=="Ventas"){
        solicitudVentas(id);
    }if(id=="Inventario"){
        solicitudInventario(id);
    }if(id=="Pedidos"){
        solicitudTipoArticulos(id);
        //solicitudPedidos(id);
    }if(id=="Historial"){
        solicitudHistorial(id);
    }if(id=="Favoritos"){
        solicitudFavoritos(id);
    }
}

function Deseleccionar(){
    document.getElementById("opcionInventario").className="opcionUnselect";
    document.getElementById("opcionHome").className="opcionUnselect";
    document.getElementById("opcionPedidos").className="opcionUnselect";
    document.getElementById("opcionHistorial").className="opcionUnselect";
    document.getElementById("opcionFavoritos").className="opcionUnselect";
    document.getElementById("opcionVentas").className="opcionUnselect";
}