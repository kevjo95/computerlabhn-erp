const enviosModel = require('../models/envios.model');

module.exports = function (app) {
    app.get('/api/envios', function (req, res) {
        data = {};
        
        enviosModel.obtenerEnvios(data, function(error, respuesta){
            if (error) {
                res.status(500).json({
                    codigo: error.errno,
                    mensaje: `${error.code} ${error.sqlMessage}`         
                });
            } else {
                res.status(200).json(respuesta);
            }     
        });
    });

    app.post('/api/envios', function (req, res) {
        data = {
            codigo: req.body.codigo,
            fecha_miami: req.body.fecha_miami,
            fecha_honduras: req.body.fecha_honduras,
            gasto_envio: req.body.gasto_envio        
        }
        
        enviosModel.insertarEnvio(data, function (error, respuesta) {
            if (error) {
                res.status(500).json({
                    codigo: error.errno,
                    mensaje: `${error.code} ${error.sqlMessage}`         
                });
            } else {
                res.status(200).json(respuesta);
            }  
        });
    });
}