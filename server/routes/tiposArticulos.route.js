const tiposArticulosModel = require('../models/tiposArticulos.model');

module.exports = function (app) {
    app.get('/api/tipos_articulos', function (req, res) {
        data = {};
        
        tiposArticulosModel.obtenerTipos(data, function (error, respuesta) {
            if (error) {
                    res.status(500).json({
                        codigo: error.errno,
                        mensaje: `${error.code} ${error.sqlMessage}`         
                    });
            } else {
                    res.status(200).json(respuesta);
            } 
        }); 
    });
}