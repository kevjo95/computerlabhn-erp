const inventarioModel = require('../models/inventario.model');

module.exports =function (app) {
    app.get('/api/inventario', function (req, res) {
        data = {};

        inventarioModel.obtenerInventario(data, function (error, respuesta) {
            if (error) {
                res.status(500).json({
                    codigo: error.errno,
                    mensaje: `${error.code} ${error.sqlMessage}`         
                });
            } else {
                res.status(200).json(respuesta);
            }   
        });
    });

    app.post('/api/inventario', function (req, res) {
        data = {
            id_tipo: req.body.id_tipo,
            id_estado: req.body.id_estado,
            id_estado_favorito: req.body.id_estado_favorito,
            articulo: req.body.articulo,
            descripcion: req.body.descripcion,
            cantidad: req.body.cantidad,
            precio_compra: req.body.precio_compra,
            precio_venta: req.body.precio_venta,
            comentarios: req.body.comentarios,
            enlace: req.body.enlace,
            id_usuario_crea: req.session.id_usuario
        }

        inventarioModel.insertarArticulo(data, function (error, respuesta) {
            if (error) {
                res.status(500).json({
                    codigo: error.errno,
                    mensaje: `${error.code} ${error.sqlMessage}`         
                });
            } else {
                res.status(200).json(respuesta);
            }
        });
    });
}