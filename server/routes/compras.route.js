const comprasModel = require('../models/compras.model');

module.exports = function (app) {
    app.get('/api/compras', function (req, res) {
        data = {};
        
        comprasModel.obtenerCompras(data, function (error, respuesta) {
            if (error) {
                res.status(500).json({
                    codigo: error.errno,
                    mensaje: `${error.code} ${error.sqlMessage}`         
                });
            } else {
                    res.status(200).json(respuesta);
            }        
        });
    });     
    
    app.post('/api/compras', function (req, res) {
        data = {
            id_envio: 1,
            id_estado_aprobacion: 2,
            codigo: req.body.codigo,
            id_usuario_crea: req.session.id_usuario            
        };
       
       comprasModel.insertarCompra(data, function (error, respuesta) {
            if (error) {
                res.status(500).json({
                    codigo: error.errno,
                    mensaje: `${error.code} ${error.sqlMessage}`         
                });
            } else {
                res.status(200).json(respuesta);
            }
       });
    });
}