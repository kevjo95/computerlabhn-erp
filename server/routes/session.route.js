const sessionModel = require('../models/session.model');

module.exports = function (app) {
    app.get('/api/session_login', function (req, res) {
        data = {
            usuario: req.query.usuario,
            contrasenia: req.query.contrasenia
        }
        
        sessionModel.validarLogin(data, function (error, respuesta) {
            if (error) {
                res.status(500).json({
                    codigo: error.errno,
                    mensaje: `${error.code} ${error.sqlMessage}`         
                });
            } else {   
                if (respuesta[0].validacion == 1) {
                    sessionModel.obtenerDatosUsuario(data, function (error, respuestaQuery) {
                        if (error) {
                            res.status(500).json({
                                codigo: error.errno,
                                mensaje: `${error.code} ${error.sqlMessage}`         
                            });
                        } else {
                            req.session.id_usuario = respuestaQuery[0].id_usuario;
                            req.session.usuario = respuestaQuery[0].usuario;
                            res.status(200).json(respuesta);
                        }
                    });
                } else {
                    res.status(200).json(respuesta);
                }
            } 
        });
    });

    app.get('/api/session_datos', function (req, res) {
        let datosUsuario = {};
        datosUsuario.id_usuario = req.session.id_usuario;
        datosUsuario.usuario = req.session.usuario;
        res.status(200).json(datosUsuario);
    });

    app.post('/api/session_logout', function (req, res) {
        req.session.destroy();
        res.status(200).json({isSession: false});
    });
}