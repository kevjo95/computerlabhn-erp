const estadosModel = require('../models/estados.model');

module.exports = function (app) {
    app.get('/api/estados', function (req, res) {
       data = {};
       
       estadosModel.obtenerEstados(data, function (error, respuesta) {
            if (error) {
                res.status(500).json({
                    codigo: error.errno,
                    mensaje: `${error.code} ${error.sqlMessage}`         
                });
            } else {
                    res.status(200).json(respuesta);
            }
       });
    });
}