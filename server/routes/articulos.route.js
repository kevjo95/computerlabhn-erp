const articulosModel = require('../models/articulos.model');

module.exports = function (app) {
    app.get('/api/articulos', function (req, res) {
        data = {};

        articulosModel.obtenerArticulos(data, function (error, respuesta) {
            if (error) {
                res.status(500).json({
                    codigo: error.errno,
                    mensaje: `${error.code} ${error.sqlMessage}`         
                });
            } else {
                res.status(200).json(respuesta);
            }  
        }); 
    });

    app.post('/api/articulos', function (req, res) {
        data = {
            articulo: req.body.articulo,
            descripcion: req.body.descripcion,
            enlace: req.body.enlace,
            id_estado: 1
        }

        articulosModel.insertarArticulo(data, function (error, respuesta) {
            if (error) {
                res.status(500).json({
                    codigo: error.errno,
                    mensaje: `${error.code} ${error.sqlMessage}`         
                });
            } else {
                res.status(200).json(respuesta);
            }
        });
    });

    app.get('/api/articulos_x_compra', function (req, res) {
        data = {
            id_compra: req.query.id_compra
        }

        articulosModel.obtenerArticulosXCompra(data, function (error, respuesta) {
            if (error) {
                res.status(500).json({
                    codigo: error.errno,
                    mensaje: `${error.code} ${error.sqlMessage}`         
                });
            } else {
                res.status(200).json(respuesta);
            } 
        });
    });

    app.post('/api/articulos_x_compra', function (req, res) {
        data = {
            id_compra: req.body.id_compra,
            id_articulo: req.body.id_articulo,
            cantidad: req.body.cantidad,
            precio_unidad: req.body.precio_unidad,
            impuesto: req.body.impuesto,
            gastos_envio: req.body.gastos_envio,
            comentario: req.body.comentario
        }

        articulosModel.insertarArticuloXCompra(data, function (error, respuesta) {
            if (error) {
                res.status(500).json({
                    codigo: error.errno,
                    mensaje: `${error.code} ${error.sqlMessage}`         
                });
            } else {
                res.status(200).json(respuesta);
            }
        });
    });
}