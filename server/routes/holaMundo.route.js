const holaMundoModel = require('../models/holaMundo.model');

module.exports = function (app){
    app.get('/hola_mundo', function (req, res) {
       data = {
           mensaje: req.query.mensaje
       }

       holaMundoModel.obtenerHolaMundo(data, function (error, respuesta) {
            if (error) {
                res.status(404).json({
                    codigo: error.errno,
                    mensaje: `${error.code} ${error.sqlMessage}`
                });
            } else {
                console.log(respuesta)
                res.status(200).json(respuesta);
            }  
       });
    });

    app.post('/hola_mundo', function (req, res) {
        data = {
            mensaje: req.body.mensaje
        }
        // console.log(req.body)
 
        holaMundoModel.obtenerHolaMundo(data, function (error, respuesta) {
             if (error) {
                 res.status(409).json({
                     codigo: error.errno,
                     mensaje: `${error.code} ${error.sqlMessage}`
                 });
             } else {
                 console.log(respuesta)
                 res.status(200).json(respuesta);
             }  
        });
     });

     app.put('/hola_mundo', function (req, res) {
        data = {
            mensaje: req.body.mensaje
        }
 
        holaMundoModel.obtenerHolaMundo(data, function (error, respuesta) {
             if (error) {
                 res.status(405).json({
                     codigo: error.errno,
                     mensaje: `${error.code} ${error.sqlMessage}`
                 });
             } else {
                 console.log(respuesta)
                 res.status(200).json(respuesta);
             }  
        });
     });

     app.delete('/hola_mundo', function (req, res) {
        data = {
            mensaje: req.body.mensaje
        }
 
        holaMundoModel.obtenerHolaMundo(data, function (error, respuesta) {
             if (error) {
                 res.status(405).json({
                     codigo: error.errno,
                     mensaje: `${error.code} ${error.sqlMessage}`
                 });
             } else {
                 console.log(respuesta)
                 res.status(200).json(respuesta);
             }  
        });
     });
}