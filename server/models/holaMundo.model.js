const conexion = require('../conexion');

var holaMundoModel = {};

holaMundoModel.obtenerHolaMundo = (data, callback) => {
    if (conexion) {
        console.log(data);
        let sql = `SELECT ${data.mensaje} as mensaje;`;

        conexion.query(
           sql,
           function (error, resultado, resgistros) {
               if (error) {
                    callback(error, null);
               } else {
                    callback(null, resultado);
               }
           } 
        )
    } else {
        callback(true, {codigo:1, mensaje:'Error al obtener Hola Mundo!'});
    }
}

module.exports = holaMundoModel;