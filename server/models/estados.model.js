const conexion = require('../conexion');

var estadosModel = {};

estadosModel.obtenerEstados = (data, callback) => {
    if (conexion) {
        let registrosConsulta = [];
        let sql = `SELECT id_estado, estado FROM tbl_estado;`;

        conexion.query(sql)
        .on('error', function (error) {
            callback(error, null);
        })
        .on('result', function (registro) {
            registrosConsulta.push(registro);
        })
        .on('end', function () {
            callback(null, registrosConsulta);
        });
    } else {
        callback(true, {codigo:1, mensaje:'Error de conexion!'});
    }
}

module.exports = estadosModel;