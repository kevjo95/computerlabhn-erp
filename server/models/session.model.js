const conexion = require('../conexion');

var sessionModel = {};

sessionModel.validarLogin = (data, callback) => {
    if (conexion) {
        // let sql = `SELECT fx_validar_login(${data.usuario}, ${data.contrasenia}) as validacion;`;
        let sql = `SELECT CASE WHEN sha1(?) = contrasenia THEN TRUE ELSE FALSE END as validacion FROM tbl_usuarios WHERE nombre_usuario = ?;`

        conexion.query(
            sql,
            [data.contrasenia, data.usuario],
            function (error, resultado, registros) {
                if (error) {
                    callback(error, null);
                } else {
                    callback(null, resultado);
                }
            }
        );
    } else {
        callback(true, {codigo:1, mensaje:'Error de conexion!'});
    }
}

sessionModel.obtenerDatosUsuario = (data, callback) => {
    if (conexion) {
        let sql = `SELECT id_usuario, nombre_usuario as usuario FROM tbl_usuarios where nombre_usuario = ?;`;

        conexion.query(
            sql,
            [data.usuario],
            function (error, resultado, registros) {
                if (error) {
                    callback(error, null);
                } else {
                    callback(null, resultado);
                }
            }
        );
    } else {
        callback(true, {codigo:1, mensaje:'Error de conexion!'});
    }
}

module.exports = sessionModel;