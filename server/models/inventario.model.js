const conexion = require('../conexion');

var inventarioModel = {};

inventarioModel.obtenerInventario = (data, callback) => {
    if (conexion) {
        let registrosConsulta = [];
        let sql = `SELECT  A.id_articulo,
                            C.tipo as 'tipo_articulo',
                            B.estado as estado,
                            D.estado as favorito,
                            A.articulo,
                            A.descripcion,
                            A.cantidad,
                            A.precio_compra,
                            A.precio_compra*1.15 as 'ganancia15',
                            A.precio_compra*1.20 as 'ganancia20',
                            A.precio_compra*1.25 as 'ganancia25',
                            A.precio_venta as 'precio_sugerido',
                            A.comentarios,
                            A.enlace,
                            A.fecha_crea,
                            A.id_usuario_crea,
                            A.fecha_modifica,
                            A.id_usuario_modifica
                    FROM tbl_inventario_art AS A
                    LEFT JOIN tbl_estado B
                    ON(A.id_estado = B.id_estado)
                    LEFT JOIN tbl_tipo_art C
                    ON(A.id_tipo = C.id_tipo)
                    LEFT JOIN tbl_estado D
                    ON(A.id_estado_favorito = D.id_estado);`;

        conexion.query(sql)
        .on('error', function (error) {
            callback(error, null);
        })
        .on('result', function (registro) {
            registrosConsulta.push(registro);
        })
        .on('end', function () {
            callback(null, registrosConsulta);
        });
    } else {
        callback(true, {codigo:1, mensaje:'Error de conexion!'});
    }
}

inventarioModel.insertarArticulo = (data, callback) => {
    if (conexion) {
        let sql = `CALL sp_insertar_articulo_inv(?,?,?,?,?,?,?,?,?,?,?);`;

        conexion.query(
            sql,
            [
                data.id_tipo,
                data.id_estado,
                data.id_estado_favorito,
                data.articulo,
                data.descripcion,
                data.cantidad == '' ? 0:data.cantidad,
                data.precio_compra == '' ? 0:data.cantidad,
                data.precio_venta == '' ? 0:data.cantidad,
                data.comentarios,
                data.enlace,
                data.id_usuario_crea
            ],
            function (error, resultado, resgistros) {
                if (error) {
                    callback(error, null);
                } else {
                    callback(null, resultado);
                }
            } 
        )
    } else {
        callback(true, {codigo:1, mensaje:'Error al obtener Hola Mundo!'});
    }
}

module.exports = inventarioModel;