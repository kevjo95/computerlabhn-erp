const conexion = require('../conexion');

var enviosModel = {};

enviosModel.obtenerEnvios = (data, callback) =>{
    if (conexion) {
        let registrosConsulta = [];
        let sql =   `SELECT id_envio,
                            codigo,
                            fecha_miami,
                            fecha_honduras,
                            gasto_envio
                    FROM tbl_envios; `;

        conexion.query(sql)
        .on('error', function (error) {
            callback(error, null);
        })
        .on('result', function (registro) {
            registrosConsulta.push(registro);
        })
        .on('end', function () {
            callback(null, registrosConsulta);
        });
    } else {
        callback(true, {codigo:1, mensaje:'Error de conexion!'});
    }
}

enviosModel.insertarEnvio = (data, callback) => {
    let sql = `CALL sp_insertar_envio(?,?,?,?);`;

    conexion.query(
        sql,
        [
            data.codigo == '' ? null : data.codigo, 
            data.fecha_miami == '' ? null : data.fecha_miami, 
            data.fecha_honduras == '' ? null : data.fecha_honduras, 
            data.gasto_envio == '' ? 0 : data.gasto_envio
        ],
        function (error, resultado, registro) {
            if (error) {
                callback(error, null);
            } else {
                callback(null, resultado);
            }
        }
    )
}

module.exports = enviosModel;