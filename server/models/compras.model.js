const conexion = require('../conexion');

var comprasModel = {};

comprasModel.obtenerCompras = (data, callback) => {
    if (conexion) {
        let registrosConsulta = [];
        let sql = `SELECT   A.id_compra,
                            A.codigo,
                            B.estado,
                            A.fecha_crea,
                            A.usuario_crea,
                            A.fecha_modifica,
                            A.usuario_modifica
                    FROM tbl_compras as A
                    LEFT JOIN tbl_estado_aprobacion B
                    ON (A.id_estado_aprobacion = B.id_estado);`;

        conexion.query(
            sql
        )
        .on('error', function (error) {
            callback(error, null);
        })
        .on('result', function (registro) {
            registrosConsulta.push(registro);
        })
        .on('end', function () {
            callback(null, registrosConsulta);
        });
    } else {
        callback(true, {codigo:1, mensaje:'Error de conexion!'});
    }
}

comprasModel.insertarCompra = (data, callback) => {
    let sql = `CALL sp_insertar_compra(?,?,?,?);`;

    conexion.query(
        sql,
        [
            data.id_envio, 
            data.id_estado_aprobacion, 
            data.codigo == '' ? null : data.codigo, 
            data.id_usuario_crea
        ],
        function (error, resultado, registros) {
            if (error) {
                callback(error, null);
            } else {
                callback(null, resultado);
            }
        }
    );
}

module.exports = comprasModel;