const conexion = require('../conexion');

var tiposArticulosModel = {};

tiposArticulosModel.obtenerTipos = (data, callback) => {
    if (conexion) {
        let registrosConsulta = [];
        let sql = `SELECT id_tipo, tipo FROM tbl_tipo_art;`;

        conexion.query(sql)
        .on('error', function (error) {
            callback(error, null);
        })
        .on('result', function (registro) {
            registrosConsulta.push(registro);
        })
        .on('end', function () {
            callback(null, registrosConsulta);
        });
    } else {
        callback(true, {codigo:1, mensaje:'Error de conexion!'});
    }
}

module.exports = tiposArticulosModel;