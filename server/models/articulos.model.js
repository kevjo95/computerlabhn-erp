const conexion = require('../conexion');

var articulosModel = {};

articulosModel.obtenerArticulos = (data, callback) => {
    if (conexion) {
        let registrosConsulta =[];
        let sql = `SELECT 	id_articulo,
                            articulo,
                            descripcion,
                            enlace,
                            id_estado
                    FROM tbl_articulos_online;`;

        conexion.query(
            sql
        )
        .on('error', function (error) {
            callback(error, null);
        })
        .on('result', function (registro) {
            registrosConsulta.push(registro);
        })
        .on('end', function () {
            callback(null, registrosConsulta);
        });
    } else {
        callback(true, {codigo:1, mensaje:'Error de conexion!'});
    }
}

articulosModel.insertarArticulo = (data, callback) => {
    if (conexion) {
        let sql = `CALL sp_insertar_articulo_online(?,?,?,?);`;

        conexion.query(
            sql,
            [
                data.articulo == '' ? null: data.articulo,
                data.descripcion == '' ? null: data.descripcion,
                data.enlace == '' ? null: data.enlace,
                data.id_estado == '' ? null: data.id_estado 
            ],
            function (error, resultado, registros) {
                if (error) {
                    callback(error, null);
                } else {
                    callback(null, resultado);
                }
            }
        )
    } else {
        callback(true, {codigo:1, mensaje:'Error de conexion!'});
    }
}

articulosModel.obtenerArticulosXCompra = (data, callback) => {
    if (conexion) {
        let registrosConsulta =[];
        let sql = `SELECT   B.id_articulo,
                            B.articulo,
                            B.descripcion,
                            A.cantidad,
                            A.precio_unidad,
                            A.impuesto,
                            A.gastos_envio,
                            A.sub_total,
                            A.comision,
                            A.total,
                            A.comentario
                    FROM tbl_art_x_compra as A
                    INNER JOIN tbl_articulos_online B
                    ON (A.id_art_online = B.id_articulo)
                    WHERE A.id_compra = ?;`;
        conexion.query(
            sql,
            [data.id_compra]
        )
        .on('error', function (error) {
            callback(error, null);
        })
        .on('result', function (registro) {
            registrosConsulta.push(registro);
        })
        .on('end', function () {
            callback(null, registrosConsulta);
        });
    } else {
        callback(true, {codigo:1, mensaje:'Error de conexion!'});
    }
}

articulosModel.insertarArticuloXCompra = (data, callback) => {
    if (conexion) {
        let sql = `CALL sp_insertar_art_x_compra(?,?,?,?,?,?,?);`;

        conexion.query(
            sql,
            [
                data.id_compra,
                data.id_articulo,
                data.cantidad,
                data.precio_unidad,
                data.impuesto,
                data.gastos_envio,
                data.comentario
            ],
            function (error, resultado, registros) {
                if (error) {
                    callback(error, null);
                } else {
                    callback(null, resultado);
                }
            }
        )
    } else {
        callback(true, {codigo:1, mensaje:'Error de conexion!'});
    }
}

module.exports = articulosModel;